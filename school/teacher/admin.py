from django.contrib import admin

from .models import Teacher

# Register your models here.
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'job', 'subject',)
    list_display_links = ('full_name',)

admin.site.register(Teacher,TeacherAdmin)
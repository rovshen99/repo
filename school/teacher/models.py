from django.db import models

from subject.models import Subject

class Teacher(models.Model):
    full_name = models.CharField(verbose_name = 'ФИО', max_length = 100)
    subject = models.ForeignKey(Subject, verbose_name='Предмет', on_delete=models.PROTECT,)
    JOB_CHOICES = (
        ('d', 'Директор'),
        ('h', 'Завуч'),
        ('t', 'Учитель'),
    )
    job = models.CharField('Должность', max_length=1, choices=JOB_CHOICES, default='t')

    def __str__(self):
        return self.full_name

    class Meta:
        verbose_name = 'Учитель'
        verbose_name_plural = 'Учители'

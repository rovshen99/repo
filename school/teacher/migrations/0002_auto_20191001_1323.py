# Generated by Django 2.2.4 on 2019-10-01 10:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='teacher',
            options={'verbose_name': 'Учитель', 'verbose_name_plural': 'Учители'},
        ),
        migrations.AlterField(
            model_name='teacher',
            name='job',
            field=models.CharField(choices=[('d', 'Директор'), ('h', 'Завуч'), ('t', 'Учитель')], default='t', max_length=1, verbose_name='Должность'),
        ),
        migrations.AlterField(
            model_name='teacher',
            name='subject',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='subject.Subject', verbose_name='Предмет'),
        ),
    ]

# Generated by Django 2.2.4 on 2019-10-01 06:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('subject', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('full_name', models.CharField(max_length=100, verbose_name='ФИО')),
                ('job', models.CharField(choices=[('d', 'Директор'), ('h', 'Завуч'), ('t', 'Учитель')], max_length=1, verbose_name='Должность')),
                ('subject', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='subject.Subject')),
            ],
        ),
    ]

from django.contrib import admin

from .models import SchoolClass

# Register your models here.
class ClassAdmin(admin.ModelAdmin):
    list_display = ('name', 'teacher_name')
    list_display_links = ('name', 'teacher_name')

admin.site.register(SchoolClass,ClassAdmin)
from django.db import models

from teacher.models import Teacher

# Create your models here.
class SchoolClass(models.Model):
    name = models.CharField(max_length=3,verbose_name='Класс')
    teacher_name = models.ForeignKey(Teacher,verbose_name='Классный руководитель', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Kласс'
        verbose_name_plural = 'Kлассы'

    def __str__(self):
        return '{} {} {}'.format(self.name, 'Классный руководитель', self.teacher_name)
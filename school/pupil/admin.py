from django.contrib import admin
from .models import Pupil

class PupilAdmin(admin.ModelAdmin):
    list_display = ('id','full_name','sex','born','age')
    list_display_links = ('full_name',)

# Register your models here.
admin.site.register(Pupil,PupilAdmin)
from django.urls import path, include

from pupil import views

urlpatterns = [
    path('', views.index, name='pupil'),
]
from django.db import models
from django.utils.timezone import now

from datetime import datetime, timedelta, date

class Pupil(models.Model):

    full_name = models.CharField(verbose_name = 'ФИО', max_length = 100, default = 'Иванов Иван')
    SEX_CHOICES =(
        ('m','мужской'),
        ('w','женский'),
    )
    sex = models.CharField(max_length=1, verbose_name='Пол', choices=SEX_CHOICES, default = 'm')
    born = models.DateField(editable=True, verbose_name='Дата Рождения', default = now)
    age = models.PositiveSmallIntegerField(null=True,blank=True,verbose_name='Возраст')

    def __str__(self):
        return str(self.full_name)

    def calculate_age(self):
        today = date.today()
        return int(today.year - self.born.year - ((today.month, today.day) < (self.born.month, self.born.day)))

    def save(self, *args, **kwargs):
        self.age = self.calculate_age()
        super(Pupil, self).save(*args,**kwargs)

    class Meta:
        verbose_name = 'Ученика'
        verbose_name_plural = 'Ученики'
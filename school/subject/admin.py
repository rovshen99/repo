from django.contrib import admin

from .models import Subject

# Register your models here.
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_display_links = ('name',)

admin.site.register(Subject,SubjectAdmin)

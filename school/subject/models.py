from django.db import models

# Create your models here.
class Subject(models.Model):
    name = models.CharField('Предмет', max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Предмет'
        verbose_name_plural = 'Предметы'

